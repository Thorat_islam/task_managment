<?php
require('../../lib/database/db_config.php');
require('../../lib/array_function.php');
require('../../lib/common_function.php');

$head_id=$_POST['head_id'];
$pill_id=$_POST['pill_id'];


$sql_menu="SELECT a.name as module,b.name as menu_name,b.icon_id FROM tbl_leftbar_head a, tbl_leftbar_menu b WHERE b.status_active=1 and b.is_delete=0 and b.head_id=$head_id and a.id=b.head_id";
//echo $sql_menu;
$module_name_arr=return_library_array( "select id, name from tbl_leftbar_head where status_active=1 and is_delete=0",'id','name');;

$result=$db->query($sql_menu);


$i=1;

    ?>
    <div class="" id="pills-<?php //echo strtolower($row['module'])?>">
        <div class="list-group list-group-flush">
            <div class="list-group-item">
                <div class="d-flex w-100 justify-content-between">
                    <h5 class="mb-0" id="pill-title"><?php echo $module_name_arr[$head_id];?></h5>
                </div>
                <small class="mb-0">Some placeholder content</small>
            </div>
            <?php foreach ($result as $row)
            {?>
                <a href="<?php echo strtolower(str_replace(' ','-',$row['menu_name']))?>" class="list-group-item">
                    <i class="<?php echo $boxicons_arr[$row['icon_id']]?>"></i>
                    <?php echo ucfirst($row['menu_name'])?>
                </a>
                <?php
            }?>
        </div>
    </div>
    <?php

?>
