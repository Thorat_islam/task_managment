<?php
require('../../lib/database/db_config.php');
require('../../lib/array_function.php');
$sql_header="SELECT id, user_id, name, icon_id FROM tbl_leftbar_head WHERE user_id=1 and status_active=1 and is_delete=0  ORDER BY sequence ASC";
//echo $sql_header;
$result=$db->query($sql_header);

$i=1;
//$iconMenu_arr=[];
foreach ($result as $key => $row)
{
    ?>
<li class='nav-item' data-bs-toggle='tooltip' data-bs-placement='right' title='<?php echo ucfirst($row['name'])?>'>
    <button onclick="leftMenubar(<?php echo $row['id']?>,'pills-<?php echo strtolower($row['name'])?>')" class='nav-link'
            data-bs-toggle='pill'
            data-bs-target='#pills-<?php echo strtolower($row['name'])?>'
            type='button'>
        <i class='<?php echo $boxicons_arr[$row['icon_id']];?>'></i>
    </button>
</li>
    <?php
}

?>
