
<aside class="sidebar-wrapper">
    <div class="iconmenu">
        <div class="nav-toggle-box">
            <div class="nav-toggle-icon"><i class="bi bi-list"></i></div>
        </div>
        <ul class="nav nav-pills flex-column" id="nav-pills">
        </ul>
    </div>
    <div class="textmenu">
        <div class="brand-logo">
            <img src="assets/images/brand-logo-2.png" width="140" alt=""/>
        </div>
        <div class="tab-content" id="tab-content">

        </div>
    </div>
</aside>
