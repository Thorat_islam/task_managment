<?php
//$sql_header="SELECT name,head_id, path FROM tbl_leftbar_menu WHERE status_active=1 and is_delete=0";
$sql_header="SELECT a.name as head,b.name,b.icon_id,b.head_id,b.path FROM tbl_leftbar_head a,tbl_leftbar_menu b WHERE a.id=b.head_id and a.status_active=1 and a.is_delete=0 and b.status_active=1 and b.is_delete=0 ";
//echo $sql_header;
$result=$db->query($sql_header);
$menu_arr=[];
foreach ($result as $val){
    $menu_arr[$val['head_id']][$val['name']]['name']=$val['name'];
    $menu_arr[$val['head_id']][$val['name']]['link']=str_replace(" ","-",strtolower($val['name']));
    $menu_arr[$val['head_id']][$val['name']]['path']=$val['path'];
}
if (isset($_GET['page']))
{
    $page=str_replace(" ","-",strtolower($_GET['page']));
    $not_found=1;
    foreach ($menu_arr as $data)
    {
       foreach ($data as $row){
//           echo "<pre>";
//           print_r($row);
//           echo "</pre>";
           if ($page==str_replace(" ","-",strtolower($row['link'])))
           {
               include($row['path']);
               $not_found=0;
           }
       }
    }
    if ($not_found)
    {
        include('pages/error/404.php');
    }
}
?>