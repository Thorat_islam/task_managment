<?php



function breadcrumb($title,$breadcrumbs=array())
{
    ?>
    <div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
        <div class="breadcrumb-title pe-3"><?php echo $title;?></div>
        <div class="ps-3">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb mb-0 p-0">
                    <?php
                    foreach ($breadcrumbs as $breadcrumb)
                    {
                        ?>
                        <li class="breadcrumb-item">
                            <a href="<?php echo $breadcrumb['href'];?>">
                                <i class="<?php echo $breadcrumb['icon'];?>"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $breadcrumb['name'];?></li>
                        <?php
                    }?>
                </ol>
            </nav>
        </div>
    </div>
<?php
}
function icon_select($values)
{
    ?>
    <div class="mb-3 w-100">
<!--        <label class="form-label">Icon Select&nbsp;-&nbsp;--><?php //echo count($values);?><!--</label>-->
        <div class="input-group">
            <select class="single-select form-select" id="icon_id">
                <option value="<?php $values[529]?>" selected disabled>
                    -----Select Icon-----
                </option>
                <?php
                    if ($values != 0){
                        foreach($values as $key => $icon)
                        {
                            ?>
                            <option value="<?php echo $key?>"><?php echo $icon?></option>
                            <?php
                        }
                    }

                ?>
            </select>
            <button class="btn btn-outline-secondary" type="button">
                <i class='bx bx-md bx-search' id="icon"></i>
            </button>
        </div>
    </div>
    <?php
}

function status_select($values)
{
    ?>
    <div class="mb-3 w-100">
        <!--        <label class="form-label">Icon Select&nbsp;-&nbsp;--><?php //echo count($values);?><!--</label>-->
        <div class="input-group">
            <select class="single-select form-select" id="status_id">
                <option value="<?php $values[1]?>" selected disabled>
                    -----Select Status-----
                </option>
                <?php
                if ($values != 0)
                {
                    foreach($values as $key => $val)
                    {
                        ?>
                        <option value="<?php echo $key?>"><?php echo $val?></option>
                        <?php
                    }
                }

                ?>
            </select>
            <button class="btn btn-outline-secondary" type="button">
                <i class='bx bx-md bx-search' id="status"></i>
            </button>
        </div>
    </div>
    <?php
}

function return_library_array($query, $id_fld_name, $data_fld_name)
{
    global $db;
    $nameArray = $db->query($query);
    foreach ($nameArray as $result) {
        $new_array[$result[$id_fld_name]] = $result[$data_fld_name];
    }
    return $new_array;
}
?>