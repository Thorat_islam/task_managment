<script>
    function leftSidebar(){
        // alert('test');
        $.ajax({
            url: 'layout/requiers/iconmenu.php',
            type: 'post',
            data: {},
            success: function (data)
            {
                $('#nav-pills').children().remove().end().append(data);
                //alert([data]);
                // let obj = JSON.parse(data);

            },
            error: function(data) {
                alert("Some Error");
            }
        });
    }
    function leftMenubar(head_id,pill_id)
    {
        //alert(head_id);
        $.ajax({
            url: 'layout/requiers/text_menu.php',
            type: 'post',
            data: { "head_id":head_id,"pill_id":pill_id},
            success: function (result) {
                $("#tab-content").children().remove().end().append(result);
            }
        });
    }
    /*=====================================
    | for icon dropdown select icon change
    |======================================
    |*/
    $('#icon_id').on("change",function ()
    {
        let icon_id=$("#icon_id").val();
        let icon = <?php echo json_encode($boxicons_arr);?>;
        $('#icon').removeClass().addClass(icon[icon_id] +" bx-md");

    });

    /*=====================================
   | for Status dropdown select icon change
   |======================================
   |*/
    $('#status_id').on("change",function ()
    {
        let status_id=$("#status_id").val();
        let status = <?php echo json_encode($boxicons_arr);?>;
        //alert(status_id);
        if(status_id == 1)
        {
            $('#status').removeClass().addClass(status[519] +" bx-md text-success");
        }else {
            $('#status').removeClass().addClass(status[520] +" bx-md text-danger");
        }
    });
    ;
    /*===================================================
    |
    | Setting Module Add Edit Delete and Select Oparetion
    |
    |=================================================== */
        $(function ()
        {
            data_list_table();
            $('#alert_msg').hide().removeClass();
            // leftSidebar();

            /*=====================================
            | For Edit Oparetion Data Load in form
            |======================================*/
            $("body").on("click","#btnEdit",function ()
            {
                let id=$(this).data("id");
                let name=$(this).data("name");
                let icon_id=$(this).data("icon_id");
                let sequence=$(this).data("sequence");
                let status_active=$(this).data("status_active");

                let icon_arr = <?php echo json_encode($boxicons_arr);?>;
                let status_arr = <?php echo json_encode($status_arr);?>;

                //alert(icon_arr[icon_id]);
                $("#txtId").val(id);
                $("#txtModuleName").val(name);
                $("#txtSequence").val(sequence);
                $("#btnSubmit").text('Update');
                $('#icon_id').children().end('option:selected').append("<option selected value='"+icon_id+"'>"+icon_arr[icon_id]+"</option");
                $('#icon').removeClass().addClass(icon_arr[icon_id] +" bx-md");
                $('#status_id').children().end('option:selected').append("<option selected value='"+status_active+"' >"+status_arr[status_active]+"</option>");

                if(status_active == 1)
                {
                    $('#status').removeClass().addClass(icon_arr[519] +" bx-md text-success");
                }else {
                    $('#status').removeClass().addClass(icon_arr[520] +" bx-md text-danger");
                }

            });
            // =====================================

            // ===========Save and Update Oparetuin
            $('#btnSubmit').on('click',function ()
            {
                let id = $('#txtId').val();
                let icon_id = $('#icon_id').val();
                let status_id = $('#status_id').val();
                let txtSequence = $('#txtSequence').val();
                let txtModuleName = $('#txtModuleName').val();
                 // alert(txtModuleName);
                if ($('#btnSubmit').text() == "Save") //==========Insert Oparetion
                {
                    // alert("Save Oparetion Working");
                    $.ajax
                    ({
                        type: "post",
                        dataType: "json",
                        data: {
                            "id":id,
                            "icon_id":icon_id,
                            "status_id":status_id,
                            "txtSequence":txtSequence,
                            "txtModuleName":txtModuleName,
                            "btnSubmit":"Submit"
                        },
                       url: "pages/setting/requiers/insert.php",
                        success:function (res)
                        {
                            clearModuleForm();
                        }
                    });
                }else if($('#btnSubmit').text() == "Update") //==========Update Oparetion
                {
                    if(txtModuleName=='Setting' && status_id==2)
                    {
                        alert('This Module DO not Inactive')
                    }else {
                        $.ajax({
                            type: "post",
                            dataType: "json",
                            data: {
                                "id":id,
                                "icon_id":icon_id,
                                "status_id":status_id,
                                "txtSequence":txtSequence,
                                "txtModuleName":txtModuleName,
                                "btnSubmit":"Submit"
                            },
                            url: "pages/setting/requiers/update.php",
                            success:function (res)
                            {
                                if(res>0)
                                {
                                    $('#alert_msg').show().removeClass().addClass('alert border-0 border-success border-start border-4 bg-light-success alert-dismissible fade show py-2');
                                    $('#alert_icon').removeClass().addClass('fs-3 text-success');

                                    $('#alert_text').removeClass().addClass('text-success').html("Data Update Is Successfully!!!");
                                    clearModuleForm();
                                    data_list_table();
                                    leftSidebar();
                                    $("#alert_msg").fadeTo(2000, 500).slideUp(500, function()
                                    {
                                        $("#alert_msg").slideUp(500);
                                    });
                                }else
                                {

                                }
                            }
                        });
                    }
                }
            });
            //=================== Module Data Select ==============

            function data_list_table()
            {
                //alert("test");
                $.ajax({
                    url: 'pages/setting/requiers/select.php',
                    type: 'post',
                    data: {},
                    success: function (result) {
                        $("#data_load").html(result);
                    }
                });
            }
            // ==================================================

            /*=========================================
           *     Sidebar Load
           * =========================================*/
            // $('#btnSetting').on('click',function (){
            //     leftSidebar();
            // });


            // ======================Form Reset=================
            $("#btnReset").on("click",function ()
            {
                clearModuleForm();
                $("#btnSubmit").text('Save');
            });
            function clearModuleForm()
            {
                $("#txtId").val(1);
                $("#txtModuleName").val("");
                $("#txtSequence").val("");
                $('#status_id').val(null).trigger('change');
                $('#icon_id').val(null).trigger('change');

                let icon_arr = <?php echo json_encode($boxicons_arr);?>;
                $('#icon').removeClass().addClass(icon_arr[529] +" bx-md");
                $('#status').removeClass().addClass(icon_arr[529] +" bx-md");

            }
        });
    /*===================================================
    | End
    |=================================================== */


</script>