<?php
require('lib/database/db_config.php');
require('lib/common_function.php');
require('lib/array_function.php');

?>
<!doctype html>
<html lang="en" class="minimal-theme">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
    <!--plugins-->
    <link href="assets/plugins/select2/css/select2.min.css" rel="stylesheet" />
    <link href="assets/plugins/select2/css/select2-bootstrap4.css" rel="stylesheet" />
    <link href="assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />

    <link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
    <link href="assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
    <link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet" />
    <!-- Bootstrap CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/css/bootstrap-extended.css" rel="stylesheet" />
    <link href="assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/icons.css" rel="stylesheet">

    <link href="assets/plugins/datatable/css/dataTables.bootstrap5.min.css" rel="stylesheet" />

    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/npm/bootstrap-icons%401.5.0/font/bootstrap-icons.css">

    <!-- loader-->
    <!--Theme Styles-->
    <link href="assets/css/dark-theme.css" rel="stylesheet" />
    <link href="assets/css/light-theme.css" rel="stylesheet" />
    <link href="assets/css/semi-dark.css" rel="stylesheet" />
    <link href="assets/css/header-colors.css" rel="stylesheet" />

    <title><?php echo ucfirst($_GET['page']);?></title>
</head>

<body onload="leftSidebar()">
<!--start wrapper-->
<div class="wrapper" id="toggled_wrapper">

    <?php
    //    <!--start top -->
    include('layout/top_navbar.php');
    //<!--end top header-->
    //<!--start sidebar -->
    include ('layout/left_sidebar.php');
    //<!--End sidebar -->
    ?>
    <!--start content-->
    <main class="page-content" id="page-content">
        <?php include('router/web.php');?>
    </main>
    <!--end page main-->
    <!--start overlay-->
    <div class="overlay nav-toggle-icon"></div>
    <!--end overlay-->
    <!--Start Back To Top Button-->
    <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
    <!--End Back To Top Button-->
    <!--start switcher-->
    <!--end switcher-->
</div>
<!--end wrapper-->

<!-- Bootstrap bundle JS -->
<script src="assets/js/bootstrap.bundle.min.js"></script>
<!--plugins-->

<!--<script src="assets/js/jquery-3.6.3.js"></script>-->
<script src="https://code.jquery.com/jquery-3.6.3.js"></script>

<script src="assets/plugins/simplebar/js/simplebar.min.js"></script>

<script src="assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js"></script>

<script src="assets/plugins/select2/js/select2.min.js"></script>
<script src="assets/js/form-select2.js"></script>


<!--<script src="assets/js/pace.min.js"></script>-->
<script src="assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js"></script>
<script src="assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="assets/plugins/apexcharts-bundle/js/apexcharts.min.js"></script>

<script src="assets/plugins/datatable/js/jquery.dataTables.min.js"></script>
<script src="assets/plugins/datatable/js/dataTables.bootstrap5.min.js"></script>
<script src="assets/js/table-datatable.js"></script>

<!--app-->
<script src="assets/js/app.js"></script>
<script src="assets/js/index.js"></script>


<script>
    new PerfectScrollbar(".best-product")
    new PerfectScrollbar(".top-sellers-list")
</script>
<?php require('lib/oparetion_js.php');?>


</body>

</html>