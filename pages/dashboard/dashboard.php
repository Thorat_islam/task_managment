<div class="row row-cols-1 row-cols-lg-2 row-cols-xl-2 row-cols-xxl-4">
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="">
                        <p class="mb-1">Total Orders</p>
                        <h4 class="mb-0 text-primary">8245</h4>
                    </div>
                    <div class="ms-auto fs-2 text-primary">
                        <i class="bi bi-cart-check"></i>
                    </div>
                </div>
                <div class="border-top my-2"></div>
                <small class="mb-0"><span class="text-success">+2.5 <i class="bi bi-arrow-up"></i></span> Compared to last month</small>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="">
                        <p class="mb-1">Total Sales</p>
                        <h4 class="mb-0 text-success">$4,562</h4>
                    </div>
                    <div class="ms-auto fs-2 text-success">
                        <i class="bi bi-piggy-bank"></i>
                    </div>
                </div>
                <div class="border-top my-2"></div>
                <small class="mb-0"><span class="text-success">+3.6 <i class="bi bi-arrow-up"></i></span> Compared to last month</small>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="">
                        <p class="mb-1">Purchase</p>
                        <h4 class="mb-0 text-pink">$9,482</h4>
                    </div>
                    <div class="ms-auto fs-2 text-pink">
                        <i class="bi bi-bag-check"></i>
                    </div>
                </div>
                <div class="border-top my-2"></div>
                <small class="mb-0"><span class="text-danger">-1.8 <i class="bi bi-arrow-down"></i></span> Compared to last month</small>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card radius-10">
            <div class="card-body">
                <div class="d-flex align-items-center">
                    <div class="">
                        <p class="mb-1">Return</p>
                        <h4 class="mb-0 text-orange">146</h4>
                    </div>
                    <div class="ms-auto fs-2 text-orange">
                        <i class="bi bi-recycle"></i>
                    </div>
                </div>
                <div class="border-top my-2"></div>
                <small class="mb-0"><span class="text-success">+3.7 <i class="bi bi-arrow-up"></i></span> Compared to last month</small>
            </div>
        </div>
    </div>
</div><!--end row-->
