<?php
$breadcrumbs=
    [
        [
                'href'=> '#',
                'icon'=> 'bx bx-home-alt',
                'name'=> '404 Erorrs ....'
        ]
    ];
echo breadcrumb('Error Page',$breadcrumbs);
?>
<div class="error-404 d-flex align-items-center justify-content-center">
    <div class="container">
        <div class="card py-5">
            <div class="row g-0">
                <div class="col col-xl-5">
                    <div class="card-body p-4">
                        <h1 class="display-1">
                            <span class="text-danger">4</span>
                            <span class="text-primary">0</span>
                            <span class="text-danger">4</span>
                        </h1>
                        <h2 class="font-weight-bold display-4">Lost in Space</h2>
                        <p>Page Not Found This Project</p>
                    </div>
                </div>
                <div class="col-xl-7">
                    <img src="assets/images/error/404-error.png" class="img-fluid" alt="">
                </div>
            </div>
            <!--end row-->
        </div>
    </div>
</div>