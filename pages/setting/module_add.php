<?php
$breadcrumbs=
    [
        [
            'href'=> '#','icon'=> 'bx bx-home-alt','name'=> 'Module Add'
        ]
    ];
echo breadcrumb('Page',$breadcrumbs);
?>
<div class="row">
    <div class="col-xl-9 mx-auto">
        <h6 class="mb-0 text-uppercase">Module Add</h6>
        <div class="card">
            <div class="card-body">

                <div id="alert_msg" class="">
                    <div class="d-flex align-items-center">
                        <div id="alert_icon" class="">
                            <i class="bi bi-x-circle-fill"></i>
                        </div>
                        <div class="ms-3">
                            <div id="alert_text" class=""></div>
                        </div>
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>

                <div class="border p-1 rounded">
                    <form autocomplete="off" enctype="multipart/form-data" id="module_form">
                        <input type="hidden" name="txtId" id="txtId" value="">
                        <div class="row mb-2">
                            <label for="txtModuleName" class="col-sm-3 col-form-label">Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="txtModuleName" placeholder="Enter Moudle Name">
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label for="icon_id" class="col-sm-3 col-form-label">Select Icon</label>
                            <div class="col-sm-9" id="select_icon">
                                <?php echo icon_select($boxicons_arr);?>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <label for="icon_id" class="col-sm-3 col-form-label">Select Status</label>
                            <div class="col-sm-9">
                                <?php echo status_select($status_arr);?>
                            </div>
                        </div>

                        <div class="row mb-2">
                            <label for="txtSequence" class="col-sm-3 col-form-label">Sequence</label>
                            <div class="col-sm-9">
                                <input type="number" name="txtSequence" id="txtSequence" class="form-control" placeholder="Sequence Number">
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9 ">
                                <button type="button" class="btn btn-primary px-2 w-25 " id="btnSubmit">Save</button>
                                <button type="reset" class="btn btn-outline-warning px-2 " id="btnReset">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xl-12">
        <h6 class="mb-0 text-uppercase">Module List</h6>
        <div class="card">
            <div class="card-body">
                <div class="border p-1 rounded">
                    <div class="table-responsive mt-3">
                        <table class="table align-middle mb-0">
                            <thead class="table-light">
                                <tr>
                                    <th style="width: 40px">Sl</th>
                                    <th style="width: 150px">Name</th>
                                    <th style="width: 40px">Icon</th>
                                    <th style="width: 80px">Sequence</th>
                                    <th style="width: 80px">Status</th>
                                    <th style="width: 100px">Create By</th>
                                    <th style="width: 120px">Create At</th>
                                    <th style="width: 100px">Update By</th>
                                    <th style="width: 120px">Update At</th>
                                    <th style="width: 120px">Action</th>

                                </tr>
                            </thead>
                            <tbody id="data_load"></tbody>

                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

