<?php
require('../../../lib/database/db_config.php');
require('../../../lib/array_function.php');
$sql="SELECT id, user_id, name, icon_id, code, sequence, status_active, is_delete, create_by, create_at, update_by, update_at FROM tbl_leftbar_head order by sequence asc";
//echo $sql;//die;
$result=$db->query($sql);
$module_data_arr=array();
foreach ($result as $val)
{
    $module_data_arr[$val['user_id']][$val['id']]=$val['id'];
    $module_data_arr[$val['user_id']][$val['name']]=$val['name'];
    $module_data_arr[$val['user_id']][$val['user_id']]=$val['user_id'];
    $module_data_arr[$val['user_id']][$val['icon_id']]=$val['icon_id'];
    $module_data_arr[$val['user_id']][$val['sequence']]=$val['sequence'];
    $module_data_arr[$val['user_id']][$val['create_by']]=$val['create_by'];
    $module_data_arr[$val['user_id']][$val['create_at']]=$val['create_at'];
    $module_data_arr[$val['user_id']][$val['update_by']]=$val['update_by'];
    $module_data_arr[$val['user_id']][$val['update_at']]=$val['update_at'];
    $module_data_arr[$val['user_id']][$val['status_active']]=$val['status_active'];
}
$i=1;
foreach ($result as $row)
{
    ?>
    <tr>
        <td><?php echo $i;?></td>
        <td><?php echo $module_data_arr[$row['user_id']][$row['name']]?></td>
        <td><span><i class="<?php echo $boxicons_arr[$module_data_arr[$row['user_id']][$row['icon_id']]];?>"></i></span></td>
        <td><?php echo $module_data_arr[$row['user_id']][$row['sequence']]?></td>
        <td>
            <?php
            if ($row['status_active']==1){
                ?>
                <span class="badge bg-light-success text-success w-100">Active</span>
                <?php
            }else{
                ?>
                <span class="badge bg-light-danger text-danger w-100">Inactive</span>
                <?php
            }

            ?>
        </td>
        <td><?php echo $module_data_arr[$row['user_id']][$row['create_by']]?></td>
        <td>
            <?php
            $date=date_create($module_data_arr[$row['user_id']][$row['create_at']]);
            echo date_format( $date,"d-M-Y");
            ?>
        </td>
        <td><?php
            if(!empty($module_data_arr[$row['user_id']][$row['update_by']])){
                echo $module_data_arr[$row['user_id']][$row['update_by']];
            }else{
                echo "--------";
            }
            ?>
        </td>
        <?php
        if (!empty($module_data_arr[$row['user_id']][$row['update_at']]))
        {
        ?>
        <td>
            <?php
            $update=date_create($module_data_arr[$row['user_id']][$row['update_at']]);
            echo wordwrap(date_format( $update,"d-M-Y H:i:s A"),11,"<br>\n");
            ?>
        </td>
        <?php
        }else{
            ?>
            <td>00-00-0000</td>

            <?php
        }
        ?>
        <td>

            <div class="btn-group">
                <button type="button" class="btn btn-outline-danger" data-id="<?php echo $row['id']?>">
                    <i class="<?php echo $boxicons_arr[273]?>"></i>
                </button>
                <button type="button" class="btn btn-outline-warning" id="btnEdit"
                        data-id="<?php echo $row['id']?>"
                        data-name="<?php echo $row['name']?>"
                        data-icon_id="<?php echo $row['icon_id']?>"
                        data-sequence="<?php echo $row['sequence']?>"
                        data-status_active="<?php echo $row['status_active']?>"
                >
                    <i class="<?php echo $boxicons_arr[163]?>"></i>
                </button>

            </div>
        </td>
    </tr>
    <?php
    $i++;
}
?>
