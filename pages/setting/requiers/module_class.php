<?php
require('../../../lib/database/db_config.php');
class ModuleClass
{
//id, user_id, name, icon_id, code, sequence, status_active, is_delete, create_by, create_at, update_by, update_at
    public $id;
    public $user_id;
    public $name;
    public $icon_id;
    public $code;
    public $sequence;
    public $status_active;
    public $is_delete;
    public $create_by;
    public $create_at;
    public $update_by;
    public $update_at;

    public function __construct($id, $user_id, $name, $icon_id, $code, $sequence, $status_active, $is_delete, $create_by, $create_at, $update_by, $update_at)
    {
        $this->id=$id;
        $this->user_id=$user_id;
        $this->name=$name;
        $this->icon_id=$icon_id;
        $this->code=$code;
        $this->sequence=$sequence;
        $this->status_active=$status_active;
        $this->is_delete=$is_delete;
        $this->create_by=$create_by;
        $this->create_at=$create_at;
        $this->update_by=$update_by;
        $this->update_at=$update_at;
    }
    function insert(){
        global $db;
        $db->query("INSERT INTO tbl_leftbar_head(user_id, name, icon_id, code, sequence, status_active, is_delete, create_by, create_at, update_by, update_at) VALUES ('$this->user_id','$this->name','$this->icon_id','$this->code','$this->sequence',' $this->status_active','$this->is_delete','$this->create_by','$this->create_at','$this->update_by','$this->update_at')");
    }
    function update()
    {
        global $db;
        $db->query("UPDATE tbl_leftbar_head SET user_id='$this->user_id',name='$this->name',icon_id='$this->icon_id',sequence='$this->sequence',status_active='$this->status_active',update_by='$this->update_by',update_at='$this->update_at' WHERE id='$this->id'");
    }
}