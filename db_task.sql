-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 22, 2023 at 02:52 AM
-- Server version: 5.7.33
-- PHP Version: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_task`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leftbar_head`
--

CREATE TABLE `tbl_leftbar_head` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `icon_id` int(11) NOT NULL,
  `code` int(11) NOT NULL DEFAULT '104',
  `sequence` int(11) NOT NULL,
  `status_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_leftbar_head`
--

INSERT INTO `tbl_leftbar_head` (`id`, `user_id`, `name`, `icon_id`, `code`, `sequence`, `status_active`, `is_delete`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(1, 1, 'Dashboard', 106, 101, 1, 1, 0, 1, '2023-01-03 08:47:48', 1, '2023-01-21 03:57:53'),
(2, 1, 'Task', 2, 102, 2, 1, 0, 1, '2023-01-03 10:36:00', 1, '2023-01-20 16:32:19'),
(3, 1, 'Setting', 260, 103, 3, 1, 0, 1, '2023-01-05 04:18:10', 1, '2023-01-20 08:23:05');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_leftbar_menu`
--

CREATE TABLE `tbl_leftbar_menu` (
  `id` int(11) NOT NULL,
  `head_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `path` varchar(245) NOT NULL,
  `icon_id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `status_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_leftbar_menu`
--

INSERT INTO `tbl_leftbar_menu` (`id`, `head_id`, `name`, `path`, `icon_id`, `code`, `status_active`, `is_delete`, `create_by`, `create_at`, `update_by`, `update_at`) VALUES
(1, 1, 'Dashboard', 'pages/dashboard/dashboard.php', 106, 101, 1, 0, 0, '2023-01-03 08:49:36', 0, '2023-01-03 14:49:36'),
(2, 2, 'Task Entry', 'pages/tasks/task_entry.php', 2, 102, 1, 0, 0, '2023-01-03 10:42:34', 0, '2023-01-03 16:42:34'),
(3, 3, 'Module Add', 'pages/setting/module_add.php', 649, 103, 1, 0, 0, '2023-01-09 06:41:26', 0, '2023-01-09 12:41:26'),
(4, 3, 'Menu Add', 'pages/setting/menu_add.php', 233, 104, 1, 0, 0, '2023-01-15 05:18:53', 0, '2023-01-15 11:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone` bigint(13) NOT NULL,
  `nid` bigint(20) NOT NULL,
  `department_id` int(11) NOT NULL,
  `designetion_id` int(11) NOT NULL,
  `per_village` longtext NOT NULL,
  `per_thana_id` int(11) NOT NULL,
  `per_districts_id` int(11) NOT NULL,
  `per_division_id` int(11) NOT NULL,
  `pre_village` longtext NOT NULL,
  `pre_thana_id` int(11) NOT NULL,
  `pre_districts_id` int(11) NOT NULL,
  `pre_division_id` int(11) NOT NULL,
  `status_active` int(11) NOT NULL,
  `is_delete` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` int(11) DEFAULT NULL,
  `update_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_leftbar_head`
--
ALTER TABLE `tbl_leftbar_head`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_leftbar_menu`
--
ALTER TABLE `tbl_leftbar_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_leftbar_head`
--
ALTER TABLE `tbl_leftbar_head`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_leftbar_menu`
--
ALTER TABLE `tbl_leftbar_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
